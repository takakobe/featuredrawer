#include "drawFeatures.h"

using namespace std;
using namespace cv;


//void draw_feature(Mat &img, const FeatureInfo &finfo, Scalar &color);
//void draw_feature2(Mat &img, const FeatureInfo &finfo, Scalar &color);

/*
Draws Lowe-type features

@param img image on which to draw features
@param feat array of Oxford-type features
@param n number of features
*/


/*
Draws a single Lowe-type feature

@param img image on which to draw
@param feat feature to be drawn
@param color color in which to draw
*/
void draw_feature_arrow(cv::Mat &img, const cv::KeyPoint &feature, const Scalar &color, int thickness)
{
	float len, hlen, blen, start_x, start_y, end_x, end_y, h1_x, h1_y, h2_x, h2_y;
	float scl, ori;
	//float scale = 5.0F * sqrt(2.0F);
	float scale = 1.F;
	float hscale = 0.75F;
	Point start, end, h1, h2;

	/* compute points for an arrow scaled and rotated by feat's scl and ori */
	start_x = feature.pt.x;
	start_y = feature.pt.y;
	scl = feature.size;
	ori = feature.angle;
	len = scl * scale;
	hlen = scl * hscale;
	blen = len - hlen;
	end_x = len * cos(ori) + start_x;
	end_y = len * sin(ori) + start_y;
	h1_x = blen * cos(ori + (float)CV_PI / 18.0F) + start_x;
	h1_y = blen * sin(ori + (float)CV_PI / 18.0F) + start_y;
	h2_x = blen * cos(ori - (float)CV_PI / 18.0F) + start_x;
	h2_y = blen * sin(ori - (float)CV_PI / 18.0F) + start_y;
	start = Point((int)start_x, (int)start_y);
	end = Point((int)end_x, (int)end_y);
	h1 = Point((int)h1_x, (int)h1_y);
	h2 = cvPoint((int)h2_x, (int)h2_y);

	line(img, start, end, color, thickness/*(int)(scl/5.0)*/, 8, 0);
	line(img, end, h1, color, thickness/*(int)(scl/5.0)*/, 8, 0);
	line(img, end, h2, color, thickness/*(int)(scl/5.0)*/, 8, 0);
}

/*
Draws a single Lowe-type feature

@param img image on which to draw
@param feat feature to be drawn
@param color color in which to draw
*/
void draw_feature_rectangle(cv::Mat &img, const cv::KeyPoint &feature, const cv::Scalar &color, int thickness)
{
	float len, x1, x2, x3, x4, y1, y2, y3, y4;
	float scl, ori, a, b;
	//float scale = 5.0F * sqrt(2.0F);
	float scale = 1.F;
	Point p1, p2, p3, p4;

	/* compute points for an arrow scaled and rotated by feat's scl and ori */
	//start_x = cvRound( feat->x );
	//start_y = cvRound( feat->y );
	scl = feature.size;

	ori = feature.angle;
	len = scl * scale;

	a = len * cos(ori);
	b = len * sin(ori);

	x1 = feature.pt.x + a + b;
	x2 = x1 - a - a;
	x3 = x2 - b - b;
	x4 = x3 + a + a;

	y1 = feature.pt.y + b - a;
	y2 = y1 - b - b;
	y3 = y2 + a + a;
	y4 = y3 + b + b;


	p1 = Point((int)x1, (int)y1);
	p2 = Point((int)x2, (int)y2);
	p3 = Point((int)x3, (int)y3);
	p4 = Point((int)x4, (int)y4);

	line(img, p1, p2, color, thickness, 8, 0);
	line(img, p2, p3, color, thickness, 8, 0);
	line(img, p3, p4, color, thickness, 8, 0);
	line(img, p4, p1, color, thickness, 8, 0);
}

void draw_feature(cv::Mat &img, const cv::KeyPoint &feature, int thickness, cv::Scalar arrow_color, cv::Scalar rect_color)
{
	draw_feature_arrow(img, feature, arrow_color, thickness);
	draw_feature_rectangle(img, feature, rect_color, thickness);
}


