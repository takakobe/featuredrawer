#include "opencv2/opencv.hpp"

//void drawFeatures(cv::Mat &img, const std::vector<FeatureInfo &featureInfos);
void draw_feature(
	cv::Mat &img, 
	const cv::KeyPoint &feature, 
	int thickness = 1, 
	cv::Scalar arrow_color = cv::Scalar(0, 255, 255), 
	cv::Scalar rect_color = cv::Scalar(0, 255, 0)
	);


